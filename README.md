# TripAppIONIC

# INSTALLATION IONIC

## 1 . Pré-requis
- NodeJS 9.x.x ( via NVM si possible)
- Atom ou VisualCode
- Serveur en local

## 2 . Installation serveur

1- Installer mongoDB.

2- Créer le répertoire /data/db à la racine du disque où mongo est installé

3- Executer la commande ```./mongod``` dans le répertoire /bin de mongoDB

4- Cloner le repository du serveur TripApp dans un autre dossier que l'application IONIC

5- éxécuter la commande ```npm install``` dans le dossier du serveur

6- ```node server.js``` 

## 3 . Installation Appli Ionic

1 - ```npm install -g ionic@3.20.0 cordova```

2 - Créer un dossier vide

3 - cloner le repository git dans ce dossier

4 - ```npm install```

5 - Démarer le serveur si ce n'est pas déjà fait

6 - ```ionic serve```


## 4 . Outils
clé GoogleMaps Android : 

clé GoogleMaps IOS : 


